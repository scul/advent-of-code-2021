def main():
    with open('input') as f:
        data = f.read().split('\n')

    data = setup(data)
    #print_grid(data)
    print(f'Part 1: {part_one(data)}')
    print(f'Part 2: {part_two(data)}')


def setup(data):
    return [list(map(int, [c for c in line])) for line in data]


def part_one(data):
    pass


def part_two(data):
    pass


def print_grid(grid):
    for line in grid:
        print(line)
    print()


if __name__ == '__main__':
    main()
