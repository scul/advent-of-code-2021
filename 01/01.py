data = []

with open('input') as f:
    data = list(map(int, f.read().split()))

i = 3
prev = 999999999999
curr = 0
inc = 0
while i <= len(data):
    curr = sum(data[i-3:i])
    if curr > prev:
        inc += 1
    prev = curr
    i += 1

print(inc)
