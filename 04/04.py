def main():
    with open('input') as f:
        data = f.read().split('\n')

    nums = list(map(int, data[0].split(',')))

    boards = setup(data)
    print(f'Part 1: {part_one(nums, boards.copy())}')
    print(f'Part 2: {part_two(nums, boards.copy())}')

def setup(data):
    boards_raw = []
    for l in data[2:]:
        if len(l) > 1:
            split = l.strip().split(' ')
            for s in split:
                if s != '':
                    boards_raw.append(int(s))

    grid = 5
    n_boards = len(boards_raw) // grid**2

    boards = [[[0 for _ in range(grid)] for _ in range(grid)] for _ in range(n_boards)]

    c = 0
    for board in boards:
        for i in range(grid):
            for j in range(grid):
                board[i][j] = boards_raw[c]
                c += 1
    return boards


def part_one(nums, boards):
    for num in nums:
        for board in boards:
            for i in range(len(board)):
                for j in range(len(board[i])):
                    if board[i][j] == num:
                        board[i][j] = 'M'
            if check_win(board):
                print('winner', num)
                return calc_score(num, board)


def part_two(nums, boards):
    win_boards = [0] * len(boards)
    win_boards[0] = 1
    for num in nums:
        for pos, board in enumerate(boards):
            for i in range(len(board)):
                for j in range(len(board[i])):
                    if board[i][j] == num:
                        board[i][j] = 'M'
            if check_win(board):
                win_boards[pos] = 1

            if 0 not in win_boards:
                return calc_score(num, board)


def calc_score(num, board):
    s = 0
    for row in board:
        for n in row:
            if n != 'M':
                s += n
    return s * num


def check_win(board):
    count_rows = [0] * len(board)
    for row in range(len(board)):
        for col in range(len(board[row])):
            if board[row][col] == 'M':
                count_rows[row] += 1
    if 5 in count_rows:
        return True

    count_cols = [0] * len(board)
    for col in range(len(board)):
        for row in range(len(board[col])):
            if board[row][col] == 'M':
                count_cols[col] += 1
    if 5 in count_cols:
        return True

    return False


if __name__ == '__main__':
    main()
