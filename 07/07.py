def main():
    with open('input') as f:
        data = f.readline()

    positions = setup(data)
    print(f'Part 1: {part_one(positions)}')
    print(f'Part 2: {part_two(positions)}')


def setup(data):
    return list(map(int, data.split(',')))


def part_one(positions):
    fuel = [9999] * max(positions)
    for h in range(max(positions)):
        fuel[h] = sum(abs(h - x) for x in positions)
    return min(fuel)


def part_two(positions):
    fuel = [9999] * max(positions)
    for h in range(max(positions)):
        fuel[h] = sum(sum(range(abs(h - x) + 1)) for x in positions)
    return min(fuel)


def print_grid(grid):
    for line in grid:
        print(line)
    print()


if __name__ == '__main__':
    main()
