def main():
    with open('input') as f:
        data = f.read().split('\n')

    data = setup(data)
    print(f'Part 1: {part_one(data)}')
    print(f'Part 2: {part_two(data)}')


def setup(data):
    return data


def part_one(data):
    opposite = {'{': '}', '[': ']', '(': ')', '<': '>'}
    points = {')': 3, ']': 57, '}': 1197, '>': 25137}
    corrupted = []
    for line in data:
        stack = []
        for c in line:
            if c in '[{(<':
                stack.append(c)
            else:
                if opposite[stack[-1]] != c:
                    corrupted.append(c)
                    break
                stack.pop(-1)

    return sum(points[p] for p in corrupted)


def part_two(data):
    opposite = {'{': '}', '[': ']', '(': ')', '<': '>'}
    points = {')': 1, ']': 2, '}': 3, '>': 4}
    score = []

    incomplete = get_incomplete_remainder(data, opposite)
    for line in incomplete:
        s = 0
        for c in reversed(line):
            s = s * 5 + points[opposite[c]]
        score.append(s)
    score.sort()
    return score[len(score)//2]


def get_incomplete_remainder(data, opposite):
    remainder = []
    for line in data:
        stack = []
        corrupt = False
        for c in line:
            if c in '[{(<':
                stack.append(c)
            else:
                if opposite[stack[-1]] != c:
                    corrupt = True
                    break
                stack.pop(-1)
        if not corrupt:
            remainder.append(stack)
    return remainder

def print_grid(grid):
    for line in grid:
        print(line)
    print()


if __name__ == '__main__':
    main()
