def main():
    with open('input') as f:
        data = f.read().split('\n')

    data = setup(data)
    print(f'Part 1: {part_one(data)}')
    print(f'Part 2: {part_two(data)}')


def setup(data):
    return [list(map(int, (x for x in d))) for d in data]


def part_one(data):
    low_points = []
    for i, row in enumerate(data):
        for j, col in enumerate(row):
            test_points = set()
            if j > 0:
                test_points.add(row[j-1])
            if j < len(row)-1:
                test_points.add(row[j+1])
            if i > 0:
                test_points.add(data[i-1][j])
            if i < len(data) - 1:
                test_points.add(data[i+1][j])
            if col < min(test_points):
                low_points.append(col)
    return sum(1+p for p in low_points)


def part_two(data):
    points = find_low(data)
    size = []
    for point in points:
        size.append(len(check_bowl(point, data)))
    size.sort()
    return mult(size[-3:])


def check_bowl(point, grid, visited=None):
    if visited is None:
        visited = set()
    x, y = point
    if x < 0 or x >= len(grid):
        return None
    if y < 0 or y >= len(grid[0]):
        return None
    if grid[x][y] == 9 or point in visited:
        return None

    visited.add(point)
    for i, j in [[-1, 0], [1, 0], [0, -1], [0, 1]]:
        v = check_bowl((x+i, y+j), grid, visited)
        if v:
            for p in v:
                visited.add(p)
    return visited


def find_low(grid):
    low_points = set()
    for i, row in enumerate(grid):
        for j, col in enumerate(row):
            test_points = set()
            if j > 0:
                test_points.add(row[j - 1])
            if j < len(row) - 1:
                test_points.add(row[j + 1])
            if i > 0:
                test_points.add(grid[i - 1][j])
            if i < len(grid) - 1:
                test_points.add(grid[i + 1][j])
            if col < min(test_points):
                low_points.add((i, j))
    return low_points


def mult(arr):
    prod = 1
    for v in arr:
        prod *= v
    return prod


def print_grid(grid):
    for line in grid:
        print(line)
    print()


if __name__ == '__main__':
    main()
