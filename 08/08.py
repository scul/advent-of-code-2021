def main():
    with open('input') as f:
        data = f.read().split('\n')

    data = setup(data)
    print(f'Part 1: {part_one(data)}')
    print(f'Part 2: {part_two(data)}')


def setup(data):
    output = []
    for line in data:
        output.append(line.split(' '))
    return output


def part_one(data):
    unique = 0
    for line in data:
        output = line[line.index('|')+1:]
        for v in output:
            l = len(v)
            if l in [2,3,4,7]:
                unique += 1
    return unique


def part_two(data):
    pass


def print_grid(grid):
    for line in grid:
        print(line)
    print()


if __name__ == '__main__':
    main()
