def main():
    with open('input') as f:
        data = f.read().split('\n')

    coords, x, y = setup(data)
    print(f'Part 1: {part_one(coords, x, y)}')
    print(f'Part 2: {part_two(coords, x, y)}')


def setup(data):
    coords = []
    x = 0
    y = 0
    for l in data:
        pair = l.split(' -> ')
        for i in range(2):
            pair[i] = tuple(map(int, pair[i].split(',')))
        coords.append(pair)
        x = max(x, max(pair[0][0], pair[1][0]))
        y = max(y, max(pair[0][1], pair[1][1]))
    return coords, x+1, y+1


def part_one(coords, m_x, m_y):
    grid = [[0] * m_y for _ in range(m_x)]

    for pair in coords:
        p_one = pair[0]
        p_two = pair[1]
        # Check horizontal
        if p_one[0] == p_two[0]:
            x = p_one[0]
            start_y = min(p_one[1], p_two[1])
            end_y = max(p_one[1], p_two[1])
            for y in range(start_y, end_y+1):
                grid[y][x] += 1

        # Check vertical
        if p_one[1] == p_two[1]:
            y = p_one[1]
            start_x = min(p_one[0], p_two[0])
            end_x = max(p_one[0], p_two[0])
            for x in range(start_x, end_x+1):
                grid[y][x] += 1
    overlap = 0
    for line in grid:
        for c in line:
            if c >= 2:
                overlap += 1
    return overlap


def part_two(coords, m_x, m_y):
    grid = [[0] * m_y for _ in range(m_x)]

    for pair in coords:
        p_one = pair[0]
        p_two = pair[1]
        # Check horizontal
        if p_one[0] == p_two[0]:
            x = p_one[0]
            start_y = min(p_one[1], p_two[1])
            end_y = max(p_one[1], p_two[1])
            for y in range(start_y, end_y + 1):
                grid[y][x] += 1

        # Check vertical
        elif p_one[1] == p_two[1]:
            y = p_one[1]
            start_x = min(p_one[0], p_two[0])
            end_x = max(p_one[0], p_two[0])
            for x in range(start_x, end_x + 1):
                grid[y][x] += 1

        # Diagonal
        else:
            x1 = p_one[0]
            x2 = p_two[0]
            y1 = p_one[1]
            y2 = p_two[1]
            x_step = 1 if x1 < x2 else -1
            y_step = 1 if y1 < y2 else -1
            n_steps = abs(x1 - x2)
            for _ in range(n_steps + 1):
                grid[y1][x1] += 1
                x1 += x_step
                y1 += y_step
    #
    # print_grid(grid)
    overlap = 0
    for line in grid:
        for c in line:
            if c >= 2:
                overlap += 1
    return overlap


def print_grid(grid):
    for line in grid:
        print(line)
    print()


if __name__ == '__main__':
    main()
