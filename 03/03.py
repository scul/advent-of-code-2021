data = []

with open('input') as f:
    data = f.read().split('\n')

o_list = data.copy()
c_list = data.copy()
l = len(data)
o = [0 for x in range(len(data[0]))]
c = o.copy()

pos = 0
while len(o_list) > 1:
    new_list = []
    for v in o_list:
        for i, d in enumerate(v):
            o[i] += 1 if d == '1' else 0
    s = sum(int(c[pos]) for c in o_list)
    for v in o_list:
        if v[pos] == str(int(s >= len(o_list)/2)):
            new_list.append(v)
    pos += 1
    o_list = new_list.copy()

pos = 0
while len(c_list) > 1:
    new_list = []
    for v in c_list:
        for i, d in enumerate(v):
            o[i] += 1 if d == '1' else 0
    s = sum(int(c[pos]) for c in c_list)
    for v in c_list:
        if v[pos] == str(int(s < len(c_list)/2)):
            new_list.append(v)
    pos += 1
    c_list = new_list.copy()
print(int(o_list[0],2) * int(c_list[0],2))