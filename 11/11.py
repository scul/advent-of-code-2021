def main():
    with open('input') as f:
        data = f.read().split('\n')

    data = setup(data)
    #print_grid(data)
    print(f'Part 1: {part_one(data)}')
    print(f'Part 2: {part_two(data)}')


def setup(data):
    return [list(map(int, [c for c in line])) for line in data]


def part_one(data):
    steps = 100
    tot_flash = 0
    prev = data.copy()
    for step in range(steps):
        new = []
        flashed = set()
        # increase energy by 1
        for i in range(len(prev)):
            new.append([])
            for j in range(len(prev[i])):
                new[i].append(prev[i][j] + 1)
        # flash if energy > 9
        flash = True
        while flash:
            flash = False
            for i in range(len(new)):
                for j in range(len(new[i])):
                    if new[i][j] > 9:
                        if (i, j) not in flashed:
                            flash = True
                            tot_flash += 1
                            flashed.add((i, j))
                            for k in [-1, 0, 1]:
                                for l in [-1, 0, 1]:
                                    if not (k == 0 and l == 0):
                                        if 0 <= i + k < len(new) and \
                                                0 <= j + l < len(new[i]):
                                            new[i + k][j + l] += 1
        # reset to 0 after flashing
        for i in range(len(new)):
            for j in range(len(new[i])):
                if new[i][j] > 9:
                    new[i][j] = 0
        prev = new.copy()
    return tot_flash


def part_two(data):
    step = 0
    tot_flash = 0
    prev = data.copy()
    while tot_flash != len(data) * len(data[0]):
        tot_flash = 0
        step += 1
        new = []
        flashed = set()
        # increase energy by 1
        for i in range(len(prev)):
            new.append([])
            for j in range(len(prev[i])):
                new[i].append(prev[i][j] + 1)
        # flash if energy > 9
        flash = True
        while flash:
            flash = False
            for i in range(len(new)):
                for j in range(len(new[i])):
                    if new[i][j] > 9:
                        if (i, j) not in flashed:
                            flash = True
                            tot_flash += 1
                            flashed.add((i, j))
                            for k in [-1, 0, 1]:
                                for l in [-1, 0, 1]:
                                    if not (k == 0 and l == 0):
                                        if 0 <= i + k < len(new) and \
                                                0 <= j + l < len(new[i]):
                                            new[i + k][j + l] += 1
        # reset to 0 after flashing
        for i in range(len(new)):
            for j in range(len(new[i])):
                if new[i][j] > 9:
                    new[i][j] = 0
        prev = new.copy()
    return step


def print_grid(grid):
    for line in grid:
        print(line)
    print()


if __name__ == '__main__':
    main()
