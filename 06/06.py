def main():
    with open('input') as f:
        data = f.readline()

    fish = setup(data)
    #print(f'Part 1: {part_one(fish)}')
    print(f'Part 2: {part_two(fish)}')


def setup(data):
    return list(map(int, data.split(',')))


def part_one(fish):
    days = 80
    for d in range(days):
        new_fish = fish.copy()
        for i, v in enumerate(fish):
            if v == 0:
                new_fish.append(8)
                new_fish[i] = 6
            else:
                new_fish[i] -= 1
        fish = new_fish.copy()
    return len(fish)


def part_two(fish):
    days = 256
    age_count = [0] * 9
    for f in fish:
        age_count[f] += 1
    for d in range(days):
        new_age = [0] * 9
        new_age[6] = age_count[0]
        for i, v in enumerate(age_count):
            new_age[i-1] += age_count[i]
        age_count = new_age.copy()
    return sum(age_count)


def print_grid(grid):
    for line in grid:
        print(line)
    print()


if __name__ == '__main__':
    main()
