data = []

with open('input') as f:
    data = f.read().split('\n')

h = 0
d = 0
aim = 0

for v in data:
    dir, n = v.split()
    if dir == 'forward':
        h += int(n)
        d += aim * int(n)
    elif dir == 'down':
        aim += int(n)
    elif dir == 'up':
        aim -= int(n)

print(h*d)
